package faith.mom_gay.mischcutils;

import com.earth2me.essentials.Essentials;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.Messenger;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class Main extends JavaPlugin {

    public static Main instance;

    @Override
    public void onEnable() {
        Messenger messenger = Bukkit.getMessenger();
        getServer().getPluginManager().registerEvents(new ConnectionListener(this), this);
        messenger.registerIncomingPluginChannel(this, "minecraft:brand", new BrandListener());
        this.getCommand("playtime").setExecutor(new CommandPlaytime(this));
        this.getCommand("ptlb").setExecutor(new CommandLeaderboard(this));
        this.getCommand("ptimer").setExecutor(new CommandPTimer(this));
        this.getCommand("wild").setExecutor(new CommandWild(this));
        this.getCommand("res").setExecutor(new CommandRevive(this));
        new CommandLeaderboard(this);
        new CommandPlaytime(this);
        new CommandPTimer(this);
        new CommandRevive(this);
        new CommandWild(this);
        Essentials ess = (Essentials) getServer().getPluginManager().getPlugin("Essentials");
        BukkitTask task = new BukkitRunnable() {

            @Override
            public void run() {
                File dir = new File(getDataFolder() + File.separator + "playerdata");
                File[] directoryListing = dir.listFiles();
                Date date = new Date();
                long t = date.getTime();
                if (directoryListing != null) {
                    for (File child : directoryListing) {
                        UUID pID = UUID.fromString(child.getName().substring(0, child.getName().length() - 4));
                        FileConfiguration playerData = YamlConfiguration.loadConfiguration(child);
                        boolean g = playerData.getBoolean("god");
                        long last = 0L;
                        if (playerData.getBoolean("dead")) {
                            last = playerData.getLong("playtime");
                        } else {
                            if (getServer().getOfflinePlayer(pID).isOnline()) {
                                long start = playerData.getLong("startTime");
                                long time = t - start;
                                long total = playerData.getLong("playtime");
                                last = total + time;
                                if (last < 1800000L && g) {
                                    ess.getUser(getServer().getPlayer(pID)).setGodModeEnabled(true);
                                } else {
                                    if (g) {
                                        try {
                                            playerData.set("god", false);
                                            playerData.save(child);
                                            ess.getUser(getServer().getPlayer(pID)).setGodModeEnabled(false);
                                            getServer().getPlayer(pID).sendMessage("your grace time has expired!");
                                            getServer().getPlayer(pID).playSound(getServer().getPlayer(pID).getLocation(), Sound.BLOCK_ANVIL_DESTROY, 10f, 1f);
                                        } catch (IOException ioe) {
                                            ioe.printStackTrace();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }.runTaskTimer(this,20*300,20*300);
    }

    @Override
    public void onDisable() {

    }
}
