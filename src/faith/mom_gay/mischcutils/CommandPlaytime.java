package faith.mom_gay.mischcutils;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class CommandPlaytime implements CommandExecutor {

    private Main main;
    public CommandPlaytime(Main main) {
        this.main = main;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
            UUID pID = p.getUniqueId();
            Date date = new Date();
            long t = date.getTime();
            File userdata = new File(main.getDataFolder(), File.separator + "playerdata");
            File f = new File(userdata, File.separator + pID + ".yml");
            FileConfiguration playerData = YamlConfiguration.loadConfiguration(f);
            if (!f.exists()) {
                return false;
                //throw new FileNotFoundException("data file not found");
            } else {
                try {
                    long last = 0L;
                    if (playerData.getBoolean("dead")) {
                        last = playerData.getLong("playtime");
                    } else {
                        long start = playerData.getLong("startTime");
                        long time = t - start;
                        long total = playerData.getLong("playtime");
                        last = total + time;
                    }
                    String formatted = String.format("%d hours, %d minutes, %d seconds",
                            TimeUnit.MILLISECONDS.toHours(last),
                            TimeUnit.MILLISECONDS.toMinutes(last) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(last)),
                            TimeUnit.MILLISECONDS.toSeconds(last) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(last))
                    );
                    sender.sendMessage("you've played for " + formatted);
                    if (playerData.getBoolean("god")) {
                        formatted = String.format("%d minutes",
                                30 - TimeUnit.MILLISECONDS.toMinutes(last)
                        );
                        sender.sendMessage("you have " + formatted + " of grace time left");
                    }
                    playerData.save(f);
                    return true;
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                    return false;
                }
            }
        }
        return false;
    }
}
