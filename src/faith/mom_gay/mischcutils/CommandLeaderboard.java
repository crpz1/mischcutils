package faith.mom_gay.mischcutils;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class CommandLeaderboard implements CommandExecutor {

    private Main main;

    public CommandLeaderboard(Main main) {
        this.main = main;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        sender.sendMessage("Playtime Leaderboard:");
        File dir = new File(main.getDataFolder() + File.separator + "playerdata");
        File[] directoryListing = dir.listFiles();
        Date date = new Date();
        long t = date.getTime();
        if (directoryListing != null) {
            for (File child : directoryListing) {
                UUID pID = UUID.fromString(child.getName().substring(0, child.getName().length() - 4));
                FileConfiguration playerData = YamlConfiguration.loadConfiguration(child);
                long last = 0L;
                if (playerData.getBoolean("dead")) {
                    last = playerData.getLong("playtime");
                } else {
                    if (sender.getServer().getOfflinePlayer(pID).isOnline()) {
                        long start = playerData.getLong("startTime");
                        long time = t - start;
                        long total = playerData.getLong("playtime");
                        last = total + time;
                    } else {
                        last = playerData.getLong("playtime");
                    }
                }
                String formatted = String.format("%d hours, %d minutes, %d seconds",
                        TimeUnit.MILLISECONDS.toHours(last),
                        TimeUnit.MILLISECONDS.toMinutes(last) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(last)),
                        TimeUnit.MILLISECONDS.toSeconds(last) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(last))
                );
                sender.sendMessage(sender.getServer().getOfflinePlayer(pID).getName() + ": " + formatted);
            }
            return true;
        } else {
            return false;
        }
    }
}
